FROM python:3.6.7
ENV PYTHONUNBUFFERED 1
EXPOSE 8000

WORKDIR /code
COPY ./requirements.txt .

RUN pip install -r requirements.txt

COPY . .

CMD ["python", "manage.py", "runserver_plus", "0.0.0.0:8000"]
