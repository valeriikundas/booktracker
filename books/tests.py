import json
import pytest
import random
import string
from datetime import datetime, timedelta

from django.contrib.auth.models import AnonymousUser, User
from django.test import Client, RequestFactory, TestCase
from unittest import skip
from django.urls import reverse

from books.models import Author, Book, ReadingDay, ReadingUser, UserBook
from books.views import get_last_page_read_by_book_name, start_reading


def random_string(n=10):
    return "".join(random.choice(string.ascii_lowercase) for i in range(10))


def random_int():
    return random.randint(0, 1000)


def create_author():
    author = Author(name=random_string())
    author.save()
    return author


def create_book():
    book = Book(title=random_string(), author=create_author(), pages_total=random_int())
    book.save()
    return book


class ReadingDayModelTests(TestCase):
    def setUp(self):
        user = User.objects.create(
            username="WillSmithPhiladelphia", password="top_secret"
        )
        self.reading_user = ReadingUser.objects.get(user=user)
        author = Author.objects.create(name="Socrates")
        book = Book.objects.create(title="book", author=author, pages_total=300)
        self.book = book

        self.user_book = UserBook.objects.create(
            reading_user=self.reading_user, book=self.book
        )

    def test_is_today_done_with_today_done(self):
        self.user_book.read_today(last_page_read=1)
        self.assertTrue(self.reading_user.is_today_done())

    def test_is_today_done_with_today_not_done(self):
        self.assertFalse(self.reading_user.is_today_done())

    def test_is_day_done(self):
        date = datetime.now() - timedelta(days=5)
        ReadingDay.objects.create(user_book=self.user_book, datetime=date)
        self.assertTrue(self.reading_user.is_day_done(date))


class BookModelTests(TestCase):
    pass


class ReadingUserModelTests(TestCase):
    def setUp(self):
        user = User.objects.create(
            username="WillSmithPhiladelphia", password="top_secret"
        )
        self.reading_user = ReadingUser.objects.get(user=user)
        author = Author.objects.create(name="Socrates")
        book = Book.objects.create(title="book", author=author, pages_total=300)
        self.book = book

        self.user_book = UserBook.objects.create(
            reading_user=self.reading_user, book=self.book
        )

    def test_get_chain_length_with_today_not_done(self):
        date = datetime.today()
        i = 0
        while i <= 10:
            date -= timedelta(days=1)
            ReadingDay.objects.create(user_book=self.user_book, datetime=date)
            i += 1

        length = self.reading_user.get_chain_length()
        assert length == 11

    def test_get_chain_length_with_today_done(self):
        date = datetime.today()
        i = 0
        while i <= 10:
            ReadingDay.objects.create(user_book=self.user_book, datetime=date)
            date -= timedelta(days=1)
            i += 1
        self.reading_user.get_chain_length()

    def test_get_book_list_for_select_field(self):
        books = self.reading_user.get_book_list_for_select_field()
        assert len(books) == 1

    def test_add_book(self):
        book2 = create_book()
        self.reading_user.add_book(book2)
        assert len(self.reading_user.get_book_list_for_select_field()) == 2


class HomeViewTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username="testuser", password="12345")

    def test_home_view(self):
        self.client.login(username="testuser", password="12345")
        response = self.client.get(reverse("books:home"))
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context["today_done"])
        self.assertEqual(response.context["chain_length"], 0)


class BookListViewTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username="testuser", password="12345")
        self.reading_user = ReadingUser.objects.get(user=self.user)
        self.author = Author.objects.create(name="name")

    def test_book_list_view_with_anonymous_user(self):
        response = self.client.get(reverse("books:list"))
        self.assertEqual(response.status_code, 200)

    def test_book_list_view_with_loggedin_user(self):
        self.client.login(username=self.user.username, password=self.user.password)
        response = self.client.get(reverse("books:list"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context["book_list"]), 0)

        Book.objects.create(title="name", author=self.author, pages_total=500)
        response = self.client.get(reverse("books:list"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context["book_list"]), 1)


class OwnBookListViewTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username="testuser", password="12345")
        self.reading_user = ReadingUser.objects.get(user=self.user)
        self.author = Author.objects.create(name="name")

    def test_own_book_list_view_with_anonymous_user(self):
        response = self.client.get(reverse("books:list_mine"))
        self.assertEqual(response.status_code, 302)

    def test_own_book_list_view_with_loggedin_user(self):
        self.client.login(username=self.user.username, password="12345")
        response = self.client.get(reverse("books:list_mine"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context["book_list"]), 0)

        book = Book.objects.create(title="name", author=self.author, pages_total=500)
        UserBook.objects.create(
            reading_user=self.reading_user, book=book, last_page_read=100
        )
        response = self.client.get(reverse("books:list_mine"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context["book_list"]), 1)

        book = Book.objects.create(title="name2", author=self.author, pages_total=500)
        UserBook.objects.create(reading_user=self.reading_user, book=book)
        response = self.client.get(reverse("books:list_mine"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context["book_list"]), 2)


class BookDetailViewTests(TestCase):
    def setUp(self):
        author = Author(name="name")
        author.save()
        book = Book(title="title", author=author, pages_total=300)
        book.save()

    def test_book_detail_view(self):
        client = Client()
        pk = Book.objects.first().pk
        response = client.get(f"/books/{pk}/")
        self.assertEqual(response.status_code, 200)


class NonViewMethodTests(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()
        self.user = User.objects.create_user(username="testuser", password="12345")
        self.reading_user = ReadingUser.objects.get(user=self.user)
        self.client.login(username="testuser", password="12345")
        self.author = Author.objects.create(name="name")
        self.book = Book.objects.create(
            title="name", author=self.author, pages_total=500
        )
        self.userbook = UserBook.objects.create(
            reading_user=self.reading_user, book=self.book
        )

    def test_get_last_page_read_by_book_name(self):
        request = self.factory.get(
            f"ajax/get_last_page_read_by_book_name?id={self.book.id}"
        )
        request.user = self.user
        response = get_last_page_read_by_book_name(request)
        self.assertEqual(response.status_code, 200)

        obj = json.loads(response.content)
        self.assertEqual(obj["last_page_read"], 0)

        ReadingDay.objects.create(
            user_book=self.userbook, datetime=datetime.today(), last_page_read=180
        )
        response = get_last_page_read_by_book_name(request)
        self.assertEqual(response.status_code, 200)

        obj = json.loads(response.content)
        self.assertEqual(obj["last_page_read"], 180)

    def test_start_reading(self):
        request = self.factory.get(
            f"ajax/get_last_page_read_by_book_name?id={self.book.id}"
        )
        request.user = self.user
        book_pk = self.book.id
        UserBook.objects.all().delete()
        start_reading(request, book_pk)

        userbook = UserBook.objects.get(reading_user=self.reading_user, book=self.book)
        self.assertEqual(userbook.book, self.book)
        self.assertEqual(userbook.reading_user, self.reading_user)


class ReadViewTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username="testuser", password="12345")
        self.reading_user = ReadingUser.objects.get(user=self.user)

    def test_zero_choices(self):
        self.client.login(username=self.user.username, password="12345")
        self.client.get("/read/")
        # TODO: finish test

    def test_few_choices(self):
        self.client.login(username=self.user.username, password="12345")
        self.client.get("/read/")
        # TODO: finish test


from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.chrome.webdriver import WebDriver


class AddBookViewTests(StaticLiveServerTestCase):
    def setUp(self):
        self.selenium = WebDriver(executable_path="./chromedriver")
        self.selenium.implicitly_wait(10)
        self.client = Client()
        self.user = User.objects.create_user(username="testuser", password="12345")
        self.reading_user = ReadingUser.objects.get(user=self.user)
        self.client.login(username=self.user.username, password="12345")

    def tearDown(self):
        self.selenium.quit()

    @pytest.mark.xfail
    def test_create_book_with_no_author(self):
        self.selenium.get(self.live_server_url + "/accounts/login/")
        username_input = self.selenium.find_element_by_id("id_username")
        username_input.send_keys(self.user.username)
        password_input = self.selenium.find_element_by_id("id_password")
        password_input.send_keys("12345")

        login_button = self.selenium.find_element_by_xpath("//button[@type='submit']")
        login_button.click()

        self.selenium.get(self.live_server_url + "/book/create/")

        title_input = self.selenium.find_element_by_id("id_title")
        title_input.send_keys("title")

        submit_button = self.selenium.find_element_by_xpath("//input[@type='submit']")
        submit_button.click()

        self.selenium.implicitly_wait(10)

        assert self.selenium.page_source.lower().find("error") == -1
