$("#id_id").change(function () {
    var id = $(this).val();
    var form = $(this).closest("form");
    $.ajax({
        url: form.attr("book-progress-url"),
        data: {
            'id': id
        },
        dataType: 'json',
        success: function (data) {
            last_page_read = data['last_page_read'];
            $("#id_last_page_read").val(last_page_read);
        },
    });
});