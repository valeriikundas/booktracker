from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import include, path

from books import views

app_name = "books"
urlpatterns = [
    path("read/", views.read, name="read"),
    path("books/", views.BookListView.as_view(), name="list"),
    path(
        "books/my/", login_required(views.OwnBookListView.as_view()), name="list_mine"
    ),
    path("book/create/", views.CreateView.as_view(), name="create"),  # FIXME:
    path("books/toread/", views.ToReadView.as_view(), name="toread"),
    path("books/<int:pk>/", views.BookDetailView.as_view(), name="detail"),  # FIXME:
    path("stats/", views.stats, name="stats"),
    path(
        "ajax/get_last_page_read_by_book_name/",
        views.get_last_page_read_by_book_name,
        name="get_last_page_read_by_book_name",
    ),
    path("start_reading/<int:book_pk>/", views.start_reading, name="start_reading"),
    path("", views.home, name="home"),
]
