from google_images_download import google_images_download


def get_cover_image(book_title, book_author):
    search_keyword = f"book {book_title} {book_author}"

    attempts = 3
    for _ in range(attempts):
        response = google_images_download.googleimagesdownload()

        absolute_image_path = response.download(
            {
                "keywords": search_keyword,
                "limit": 1,
                "no_download": True,
                "socket_timeout": 30,
            }
        )
        results = absolute_image_path[0][search_keyword]
        if results:
            url = results[0]
        else:
            continue

        return url

    return None
