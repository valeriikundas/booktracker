from django.contrib import admin
from django.contrib.auth.models import User
from .models import Author, BookCategory, Book, ReadingUser, UserBook, ReadingDay, Note


class ReadingUserInline(admin.StackedInline):
    model = ReadingUser
    can_delete = False


class BookInline(admin.TabularInline):
    model = Book
    extra = 0
    fields = ["image"]
    readonly_fields = ["image"]
    can_delete = False


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ("id", "name")
    search_fields = ("name",)
    inlines = [BookInline]


@admin.register(BookCategory)
class BookCategoryAdmin(admin.ModelAdmin):
    list_display = ("id", "category")


class NoteInline(admin.StackedInline):
    model = Note
    extra = 1


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "title",
        "author",
        "pages_total",
        "image",
        "image_url",
        "added_by",
    )
    list_filter = ("author", "added_by")
    raw_id_fields = ("categories",)


class ReadingUserAdmin(admin.ModelAdmin):
    list_display = ("id", "user")
    list_filter = ("user",)
    raw_id_fields = ("books",)


@admin.register(UserBook)
class UserBookAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "reading_user",
        "book",
        "last_page_read",
        "start_read_date",
        "end_date_read",
        "review",
    )
    list_filter = ("reading_user", "book", "start_read_date", "end_date_read")
    inlines = [NoteInline]


@admin.register(ReadingDay)
class ReadingDayAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "user_book",
        "datetime",
        "last_page_read",
        "time_read",
        "pages_read",
    )
    list_filter = ("user_book", "datetime")
    readonly_fields = ["datetime"]


@admin.register(Note)
class NoteAdmin(admin.ModelAdmin):
    list_display = ("id", "userbook", "text")
    list_filter = ("userbook",)


from django.contrib.auth.admin import UserAdmin as BaseUserAdmin


class UserAdmin(BaseUserAdmin):
    inlines = (ReadingUserInline,)


admin.site.unregister(User)
admin.register(User, UserAdmin)
admin.site.register(ReadingUser)
