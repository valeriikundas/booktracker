from calendar import HTMLCalendar

from django.utils.safestring import mark_safe

from .models import ReadingDay, UserBook


class Calendar(HTMLCalendar):
    def __init__(self, user, year, month):
        self.user = user
        self.year = year
        self.month = month
        super(Calendar, self).__init__()

    # formats a day as td
    def formatday(self, day, events):
        color_style = ""
        if len(events.filter(datetime__day=day)) > 0:
            color_style = "style='background-color: #20CC82'"
        if day == 0:
            day = " "
        return f"<td {color_style}>{day}</td>"

    # formats a week as tr
    def formatweek(self, week, events):
        html = f"<tr>"
        for day in week:
            html += f"{self.formatday(day,events)}"
        html += f"</tr>"
        return html

    # formats a month as table
    def formatmonth(self):
        reading_days = ReadingDay.objects.filter(
            user_book__reading_user__pk=self.user.pk,
            datetime__year=self.year,
            datetime__month=self.month,
        )

        html = (
            f'<div class="table-responsive"><table class="table table-sm text-center">'
        )
        html += f"<div>{self.formatmonthname(self.year, self.month, True)}</div>"
        html += f"{self.formatweekheader()}"
        for week in self.monthdayscalendar(self.year, self.month):
            html += f"{self.formatweek(week,reading_days)}"
        html += f"</table></div>"
        return mark_safe(html)
