from django import forms

from .models import Author, Book, UserBook, BookCategory


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        exclude = ["image", "image_url", "author", "added_by"]

    author_field = forms.CharField(
        label="Author", widget=forms.TextInput(), required=False
    )  # TODO:autocomplete field
    categories = forms.ModelMultipleChoiceField(
        BookCategory.objects.order_by("category"),
        required=False,
        help_text="Hold down “Control”, or “Command” on a Mac, to select more than one.",
    )
    pages_total = forms.IntegerField(min_value=0, required=False)

    field_order = ["title", "author_field", "pages_total"]


# FIXME:refactor to ModelForm
class ReadBookForm(forms.Form):
    id = forms.ChoiceField(label="title")
    last_page_read = forms.IntegerField(label="last page read", min_value=0)
    time_read = forms.IntegerField(label="time read", min_value=0, required=False)
