from django import forms
from django.contrib import messages
from django.contrib.auth import authenticate, login, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm, UserCreationForm
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.db.models import Max, Q, F
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import (
    get_list_or_404,
    get_object_or_404,
    redirect,
    render,
    reverse,
)
from django.urls import reverse_lazy
from django.utils import timezone
from django.views import generic

from books.calendar_widget import Calendar
from books.forms import BookForm, ReadBookForm
from books.models import Author, Book, Note, ReadingDay, ReadingUser, UserBook
from books.utils import get_cover_image

from books.models import BookCategory


def get_reading_user(user):
    return ReadingUser.objects.filter(user=user).first()


def home(request):
    if request.user.is_authenticated:
        timenow = timezone.now()
        year = timenow.year
        month = timenow.month
        calendar = Calendar(request.user, year, month).formatmonth()

        if request.user.is_authenticated:
            reading_user = get_reading_user(request.user)
            today_done = reading_user.is_today_done()
            chain_length = reading_user.get_chain_length()
        else:
            today_done = False
            chain_length = 0

        return render(
            request,
            "books/home.html",
            {
                "calendar": calendar,
                "today_done": today_done,
                "chain_length": chain_length,
            },
        )
    else:
        return render(request, "books/cover.html")


@login_required
def read(request):
    if request.method == "GET":
        reading_user = get_reading_user(request.user)

        choices = reading_user.get_book_list_for_select_field()
        book_select_field = forms.ChoiceField(widget=forms.Select, choices=choices)

        today_done = reading_user.is_today_done()
        chain_length = reading_user.get_chain_length()

        last_page_read = None
        if len(choices):
            book = Book.objects.get(pk=choices[0][0])
            userbook = UserBook.objects.get(reading_user=reading_user, book=book)
            last_page_read = userbook.last_page_read

        form = ReadBookForm(initial={"last_page_read": last_page_read})
        form.fields["id"].choices = choices

        return render(
            request,
            "books/read_form.html",
            {
                "book_list": book_select_field,
                "today_done": today_done,
                "chain_length": chain_length,
                "form": form,
            },
        )
    else:
        book_id = request.POST.get("id")
        last_page_read = int(request.POST.get("last_page_read"))
        time_read = int(request.POST.get("time_read"))

        mark_as_read_today(request.user, book_id, last_page_read, time_read)

        return redirect("books:home")


def mark_as_read_today(user, book_id, last_page_read, time_read):
    reading_user = ReadingUser.objects.get(user=user)
    userbook = UserBook.objects.get(reading_user__user=user, book__id=book_id)
    userbook.read_today(last_page_read, time_read)


@login_required
def get_last_page_read_by_book_name(request):
    id = request.GET["id"]
    book = get_object_or_404(Book, id=id)
    readinguser = ReadingUser.objects.get(user=request.user)
    userbook = UserBook.objects.get(reading_user=readinguser, book=book)
    last_page_read = userbook.last_page_read
    data = {"last_page_read": last_page_read}
    return JsonResponse(data)


# TODO:remove this. leave only `my books` page created by me
class BookListView(generic.ListView):
    model = Book
    template_name = "books/list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.user.is_authenticated:
            context["today_done"] = ReadingUser.objects.get(
                user=self.request.user
            ).is_today_done()
        else:
            context["today_done"] = True
            # TODO:rename today_done -> show_read_today_banner

        category = self.request.GET.get("category")
        if category:
            # FIXME:change to self.object_list
            context["book_list"] = Book.objects.filter(categories__category=category)
        else:
            context["book_list"] = Book.objects.all()

        context["categories"] = BookCategory.objects.all()

        return context


class OwnBookListView(generic.ListView):
    model = UserBook
    template_name = "books/list_mine.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            reading_user = get_reading_user(self.request.user)
            context["today_done"] = reading_user.is_today_done()
            context["chain_length"] = reading_user.get_chain_length()

            reading_list = self.object_list.filter(
                reading_user__user=self.request.user
            ).values_list("book", flat=True)

            category = self.request.GET.get("category")
            book_list = Book.objects.all()
            if category:
                book_list = Book.objects.filter(categories__category=category)

            # book_list = book_list.filter(pk__in=reading_list)

            book_list = book_list.filter(
                Q(added_by__isnull=True) | Q(added_by=reading_user)
            )  # FIXME: remove Q(added_by=None) part when all books have authors

            # FIXME:
            # context["book_list"] = book_list #FIXME:change this to this style
            context["book_list"] = [
                {
                    "title": book.title,
                    "image": book.image,
                    "image_url": book.image_url,
                    "pk": book.id,
                    "author": book.author,
                    "progress": get_reading_progress(self.request.user, book.title),
                }
                for i, book in enumerate(book_list)
            ]

            context["categories"] = BookCategory.objects.all()

        return context


def get_reading_progress(user, book_title):
    user_book = UserBook.objects.filter(
        Q(reading_user__user=user), Q(book__title=book_title)
    ).first()
    if not user_book:
        return None
    return user_book.progress


class BookDetailView(generic.UpdateView):
    template_name = "books/detail.html"
    model = UserBook

    def get(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        book = get_object_or_404(Book, pk=pk)

        today_done = None
        review = None
        note_list = None
        last_page_read_form = None
        reading_status = None

        if request.user.is_authenticated:
            userbook = UserBook.objects.filter(
                reading_user=get_reading_user(request.user), book__title=book.title
            ).first()

            if userbook:
                last_page_read = userbook.last_page_read
                note_list = userbook.note_set.all().values_list(("text"), flat=True)
                review = userbook.review  # TODO:make review a model
                reading_user = get_reading_user(request.user)
                today_done = reading_user.is_today_done()
                reading_status = userbook.status

        return render(
            request,
            self.template_name,
            {
                "title": book.title,
                "author": book.author,
                "pages_total": book.pages_total,
                "cover_src": book.image_url,
                "today_done": today_done,
                "book_pk": book.id,
                "reading_status": reading_status,
                "note_list": note_list,
                "review": review,
            },
        )

    def post(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        book = get_object_or_404(Book, pk=pk)

        userbook, created = UserBook.objects.get_or_create(
            reading_user__user=request.user, book=book
        )

        last_page_read = request.POST.get("last_page_read", None)
        if last_page_read is not None:
            userbook.last_page_read = last_page_read
            userbook.save()

        newnote = request.POST.get("newnote", None)
        if newnote:
            newnote = Note.objects.create(userbook=userbook, text=newnote)
            userbook.note_set.add(newnote)

        review = request.POST.get("review", None)
        if review:
            userbook.review = review
            userbook.save()

        return redirect("books:detail", pk=pk)


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data["username"]
            raw_password = form.cleaned_data["password1"]
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect("books:home")
    else:
        form = UserCreationForm()
    return render(request, "registration/signup.html", {"form": form})


def profile(request):
    return render(request, "books/profile.html", {})


def change_password(request):
    if request.method == "POST":
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, "Password successfully changed.")
            return redirect("password_change_done")
        else:
            messages.error(request, "Please correct the error below")
    else:
        form = PasswordChangeForm(request.user)
    return render(request, "books/password_change_form.html", {"form": form})


# TODO:https://getbootstrap.com/docs/4.3/components/modal/

# TODO:Filtering in catalog
# TODO:Quotes on first page
# TODO:Favorite books
# TODO:Thoughts during reading and after


@login_required
def start_reading(request, book_pk):
    book = get_object_or_404(Book, pk=book_pk)
    reading_user = ReadingUser.objects.get(user=request.user)
    UserBook.objects.create(reading_user=reading_user, book=book)
    return redirect(reverse("books:detail", kwargs={"pk": book_pk}))


# TODO:Genericforeignkey
# TODO: Modelmanager
# TODO: Ajax .success,.error,.complete
# TODO: Select related


class CreateView(generic.CreateView):
    template_name = "books/create_book_form.html"
    form_class = BookForm
    model = Book

    def get_success_url(self):
        return reverse_lazy("books:detail", args=[self.object.pk])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["today_done"] = ReadingUser.objects.get(
            user=self.request.user
        ).is_today_done()
        return context

    def form_valid(self, form):
        author_new = form.cleaned_data["author_field"]
        if author_new:
            author, _ = Author.objects.get_or_create(name=author_new)
            form.instance.author = author

        title = form.cleaned_data["title"].encode("ascii", "replace").decode()

        # TODO:move to celery
        form.instance.image_url = get_cover_image(title, form.instance.author or "")

        form.instance.added_by = get_reading_user(self.request.user)

        return super().form_valid(form)


class ToReadView(generic.ListView):
    model = UserBook
    template_name = "books/toread_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            reading_user = ReadingUser.objects.get(user=self.request.user)
            context["today_done"] = reading_user.is_today_done()
            context["chain_length"] = reading_user.get_chain_length()

            reading_list = self.object_list.filter(
                reading_user__user=self.request.user, last_page_read=0
            ).values_list("book", flat=True)
            context["book_list"] = Book.objects.filter(pk__in=reading_list).all()

            book_list = Book.objects.filter(pk__in=reading_list)
            context["book_list"] = [
                dict(
                    title=book.title,
                    image=book.image,
                    pk=book.id,
                    author=book.author,
                    progress=get_reading_progress(self.request.user, book.title),
                )
                for i, book in enumerate(book_list)
            ]

        return context


def stats(request):
    reading_days = ReadingDay.objects.filter(
        user_book__reading_user__user=request.user
    ).order_by("-datetime")
    context = {"days": reading_days}
    return render(request, "books/stats.html", context)
