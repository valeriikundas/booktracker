import datetime

from django.contrib.auth.models import AbstractUser, User
from django.db import models
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.db.models import Max


class Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name


class BookCategory(models.Model):
    category = models.CharField(max_length=50, unique=True)

    class Meta:
        verbose_name_plural = "Book categories"

    def __str__(self):
        return self.category


class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(Author, on_delete=models.CASCADE, null=True)
    pages_total = models.PositiveSmallIntegerField(null=True)
    image = models.ImageField(upload_to="covers", null=True, blank=True)
    image_url = models.CharField(null=True, max_length=1000)
    categories = models.ManyToManyField(BookCategory, blank=True)
    added_by = models.ForeignKey(
        "ReadingUser", on_delete=models.CASCADE, null=True
    )  # FIXME:remove null=True after adding authorship to all books

    class Meta:
        unique_together = ("title", "author")

    def __str__(self):
        return f"{self.title} - {self.author}"


class ReadingUser(models.Model):  # TODO:rename to reader
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    books = models.ManyToManyField(Book, through="UserBook")

    def __str__(self):
        return f"{self.user}"

    def is_day_done(self, date):
        days = ReadingDay.objects.filter(
            user_book__reading_user=self, datetime__date=date
        )
        return days.count() > 0

    def is_today_done(self):
        return (
            ReadingDay.objects.filter(
                user_book__reading_user__pk=self.pk,
                datetime__date=datetime.date.today(),
            ).count()
            > 0
        )

    def get_chain_length(self):
        length = 0
        iterdate = datetime.date.today()
        if self.is_today_done():
            length += 1

        delta = datetime.timedelta(days=1)
        iterdate -= delta
        while self.is_day_done(iterdate):
            length += 1
            iterdate -= delta
        return length

    def add_book(self, book):
        self.books.add(book)

    def get_book_list_for_select_field(self):
        userbook_list = UserBook.objects.filter(reading_user__user=self.user)
        book_list = [i.book for i in userbook_list]
        choices = [(i.id, i.title) for i in book_list]
        return choices


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        ReadingUser.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.readinguser.save()


class UserBook(models.Model):
    reading_user = models.ForeignKey(ReadingUser, on_delete=models.CASCADE, null=True)
    book = models.ForeignKey(Book, on_delete=models.CASCADE, null=True)
    last_page_read = models.PositiveSmallIntegerField(default=0)
    start_read_date = models.DateField(auto_now=True)
    end_date_read = models.DateField(auto_now_add=True)
    review = models.TextField()

    class Meta:
        unique_together = ("reading_user", "book")

    def __str__(self):
        username = None
        title = None
        try:
            username = self.reading_user.user.username
        except:
            username = "username"
        try:
            title = self.book.title
        except:
            title = "title"
        return f"{username} reads {title}"

    def read_today(self, last_page_read, time_read=None):
        today = datetime.date.today()
        ReadingDay.objects.create(
            user_book=self,
            datetime=today,
            last_page_read=last_page_read,
            time_read=time_read,
        )

    @property
    def progress(self):
        if not self.book.pages_total:
            return None
        if not self.last_page_read:
            return None
        return round(self.last_page_read / self.book.pages_total * 100)

    READ = "READ"
    WANT_TO_READ = "WANT TO READ"
    CURRENTLY_READING = "CURRENTLY READING"

    @property
    def status(self):
        if self.last_page_read == 0:
            return self.WANT_TO_READ
        if self.last_page_read >= self.book.pages_total:
            return self.READ
        return self.CURRENTLY_READING


class ReadingDay(models.Model):
    user_book = models.ForeignKey(UserBook, on_delete=models.CASCADE)
    datetime = models.DateTimeField()
    last_page_read = models.PositiveSmallIntegerField(default=0)
    time_read = models.IntegerField(default=0, null=True)
    pages_read = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return f"{self.user_book.reading_user} read til {self.last_page_read} pages of {self.user_book.book} on {self.datetime}"

    def get_user(self):
        return self.user_book.reading_user

    def get_book(self):
        return self.user_book.book

    def save(self, *args, **kwargs):
        self.user_book.last_page_read = self.last_page_read
        self.user_book.save()

        reading_day = (
            ReadingDay.objects.filter(user_book=self.user_book)
            .order_by("-datetime")
            .first()
        )
        if reading_day:
            past_last_page_read = reading_day.last_page_read
        else:
            past_last_page_read = 0

        self.pages_read = self.last_page_read - past_last_page_read

        super().save(*args, **kwargs)


class Note(models.Model):
    userbook = models.ForeignKey(UserBook, on_delete=models.CASCADE)
    text = models.TextField()

    def __str__(self):
        return f"{self.text}"

    # def save(self, *args, **kwargs):
    #     super().save(*args, **kwargs)
