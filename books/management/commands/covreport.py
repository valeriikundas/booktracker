import os

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "initializes some data in database"

    def handle(self, *args, **kwargs):
        cmd = "coverage html && google-chrome htmlcov/index.html"
        os.system(cmd)
