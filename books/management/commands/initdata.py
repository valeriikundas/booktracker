from datetime import datetime, timedelta

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.db import transaction

from books.models import Author, Book, ReadingDay, ReadingUser, UserBook


class Command(BaseCommand):
    help = "initializes some data in database"

    def handle(self, *args, **kwargs):
        with transaction.atomic():
            user1 = User.objects.create(username="Will_Smith_Bro", password="secret")
            user2 = User.objects.create(username="John Snow_LA", password="thecool")

            readinguser1 = ReadingUser.objects.get(user=user1)
            readinguser2 = ReadingUser.objects.get(user=user2)

            author1 = Author.objects.create(name="Ayn Rand")
            book1 = Book.objects.create(
                title="Atlas Shrugged", author=author1, pages_total=300
            )

            author2 = Author.objects.create(name="Steven Levitt")
            book2 = Book.objects.create(
                title="Freakonomics", author=author2, pages_total=250
            )

            userbook1 = UserBook.objects.create(reading_user=readinguser1, book=book2)
            userbook2 = UserBook.objects.create(reading_user=readinguser2, book=book1)

            ReadingDay.objects.create(
                user_book=userbook1, date=datetime.today(), last_page_read=40
            )
            ReadingDay.objects.create(
                user_book=userbook2,
                date=datetime.today() - timedelta(days=2),
                last_page_read=40,
            )
