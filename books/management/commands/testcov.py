import os

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "initializes some data in database"

    def handle(self, *args, **kwargs):
        cmd = "coverage run --source='.' manage.py test --failfast"
        os.system(cmd)
