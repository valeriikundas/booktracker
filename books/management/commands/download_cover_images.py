from django.core.management.base import BaseCommand
from django.db import transaction

from books.models import Book
from books.utils import download_cover_image


class Command(BaseCommand):
    help = "initializes images for books where it does not exist"

    def handle(self, *args, **kwargs):
        with transaction.atomic():
            book_list = Book.objects.all()  # .exclude(image=None)

            for book in book_list:
                download_cover_image(book)
